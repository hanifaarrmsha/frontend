import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {Grid, Row, Col, Clearfix} from 'react-bootstrap';

export default class Card extends Component{
    render(){
        return(
            <Col sm={6} md={3}>
                <div style={{margin:'1em'}}>
                    <img width='75' src={this.props.avatar_url} alt="hehe"/>
                    <div style={{display:'inline-block'}}>
                        <div style={{fontSize:'1.25em', fontWeight:'bold'}}>{this.props.name}</div>
                        <div>{this.props.company}</div>
                    </div>
                </div>
            </Col>
            // <Clearfix visibleSmBlock></Clearfix>
        );
    }
};